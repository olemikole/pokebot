#include <iostream>
#include <nlohmann/json.hpp>
#include <vector>
#include <restclient-cpp/restclient.h>


using json = nlohmann::json;

struct Pokemon
{
    uint16_t id;
    std::vector<std::string> types;
    std::string sprite_url;
    std::string name;
};

Pokemon fromId(uint16_t id)
{
    auto response = RestClient::get("https://pokeapi.co/api/v2/pokemon/"+std::to_string(id));
    auto res = json::parse(response.body);
    Pokemon p;
    p.id = id;
    p.name = res["name"];
    p.sprite_url=res["sprites"]["front_default"];
    auto types = res["types"];
    for(int i=0;i < types.size();i++)
    {
        p.types.push_back(types[i]["type"]["name"]);
    }
    return p;
}

bool checkIfExist(uint16_t id)
{
    //Since the id of the pokemon is unrelated to the id in the database, all the data must be fetched
    auto response = RestClient::get("http://127.0.0.1:5000/fetch/all");

    auto json_list = json::parse(response.body);

    for(auto i: json_list)
    {

        std::string pokemon_string = i["data"];
        json pokemon_json=json::parse(pokemon_string);

        if( pokemon_json["id"] == id)
        {
            return true;
        }
    }
    return false;
}

void postPokemon(Pokemon pokemon)
{
    
    json pokemon_json;
    pokemon_json["id"] = pokemon.id;
    pokemon_json["name"] = pokemon.name;
    pokemon_json["types"] = pokemon.types;
    pokemon_json["url"] = pokemon.sprite_url;
    
    json json_object;
    json_object["data"]=pokemon_json.dump();
    json_object["meta"]="pokemon";

    auto response = RestClient::post("http://127.0.0.1:5000/submit","application/json",json_object.dump());

}

int main(int argc, char** argv) 
{
    srand(time(NULL));
    uint16_t id = random()%898+1;

    auto pokemon = fromId(id);
    std::cout<<"You caught a "+pokemon.name << std::endl;
    
    if (checkIfExist(id))
    {
        std::cout<<"You already have this.\n";
    }
    else
    {
        std::cout<<"This is your first time cathching this.\n";
        postPokemon(pokemon);
    }
}