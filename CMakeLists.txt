cmake_minimum_required(VERSION 3.0)
project(rest_test)


add_executable(pokebot src/main.cpp)




# ---_ VCPKG INCLUDE AND LINK DIRECOTRIES ----
INCLUDE_DIRECTORIES( /opt/vcpkg/installed/arm64-linux/include )
LINK_DIRECTORIES( /opt/vcpkg/installed/arm64-linux/lib )


# ---------------- Restclient ----------------
find_package(restclient-cpp CONFIG REQUIRED)

# ------------------ Json --------------------
find_package(nlohmann_json CONFIG REQUIRED)


TARGET_LINK_LIBRARIES(
    pokebot
    restclient-cpp 
    nlohmann_json
)
