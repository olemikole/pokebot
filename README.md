# pokebot
Program that gets a random pokemon from [PokeApi](https://pokeapi.co) and posts it on a python server.
## Usage
[Python Generic DB REST Server](https://gitlab.com/noroff-accelerate/embedded-bootcamp/projects/) must be running on port 5000 before executing the program.
```bash
# Create and enter build directory
$ mkdir build
$ cd build

# Build project
$ cmake .. 
$ make

# Run program
$ ./pokebot
```

## Requirements
`cmake`, `make` and `g++`
### Required Libraries
[restclient-cpp](https://github.com/mrtazz/restclient-cpp)

[nlohmann/json](https://github.com/nlohmann/json)


## Contributors
[Karl Munthe (@karl.munthe)](https://gitlab.com/karl.munthe)

[Kai Chen (@kjchen93)](https://gitlab.com/kjchen93)

[Ole Kjepso (@olemikole)](https://gitlab.com/olemikole)